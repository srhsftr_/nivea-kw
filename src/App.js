import React from 'react';
import './App.css'; 
import logo from './assets/logo.png';
import gambar from './assets/1.jpg';
import gambar2 from './assets/2.jpeg';
import gambar3 from './assets/3.jpg';
import produk from './assets/produk.PNG';
import produk2 from './assets/produk2.PNG';
import c1 from './assets/c1.png';
import c2 from './assets/c2.png';
import c3 from './assets/c3.jpg';
import c4 from './assets/c4.png';
import c5 from './assets/c5.jpg';
import c6 from './assets/c6.png';
import cookies from './assets/cookies.png';
import search from './assets/search.png';
import sejarah from './assets/sejarah.PNG';
import sejarah2 from './assets/sejarah2.PNG';
import {
Nav,
Navbar,
Form,
Container,
Carousel,
Col,
NavDropdown,
Card,
Jumbotron,
Row
}from 'react-bootstrap';

function Header() {
  return(
    <Navbar className="nav"  expand="lg">
      <Container>
        <Navbar.Brand href="#home" inline>
          <img
            src={logo}
            width="65"
            height="65"
            className="d-inline-block align-top"
            alt="logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavDropdown title="PRODUK" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#link">SARAN</Nav.Link>
            <Nav.Link href="#highlights">HIGHLIGHTS</Nav.Link>
            <Nav.Link href="#dunia.nivea">DUNIA NIVEA</Nav.Link>
          </Nav>
          <Form inline>
            <Nav.Link href="#search"><img src={search} alt="search"/></Nav.Link>
            <Nav.Link href="#cookies"><img src={cookies} alt="cookies"/></Nav.Link>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}


function Slide() {
  const Welcome = (props) => {
    return(
      <Carousel>
      <Carousel.Item interval={1500}>
        <img
          className="d-block w-100"
          src={props.gambar}
          alt="First slide"
        />
      </Carousel.Item>
      </Carousel>
    );
  }
  return(
    <Row>
      <Col>
        <Welcome
        gambar= {gambar}        
        />
      </Col>
    </Row>
    //   <Carousel.Item interval={1500}>
    //     <img
    //       className="d-block w-100"
    //       src={gambar2}
    //       alt="Third slide"
    //     />
    //   </Carousel.Item>
    //   <Carousel.Item interval={1500}>
    //     <img
    //       className="d-block w-100"
    //       src={gambar3}
    //       alt="Third slide"
    //     />
    //   </Carousel.Item>
    // </Carousel>
  );
}

function Tentang(){
  return(
    <Jumbotron fluid className="mb-0" style={{backgroundColor:"#f5f5f5"}}>
        <div style={{textAlign:"center"}}>
          <h2 style={{color:"darkblue", textAlign:"center", marginBottom:"3"}}><b>HAL, APA YANG KAMU CARI?</b></h2>
        </div>
        <Row>
          <Col>
            <Card className="tentang mr-2" style={{ width: '9rem'}}>
              <Card.Img variant="top" src={produk}/>
            </Card>
          </Col>
          <Col>
            <Card className="tentang ml-2"style={{ width: '9rem'}}>
              <Card.Img variant="top" src={produk2}/>
            </Card>
          </Col>
        </Row>
    </Jumbotron>
  );
}

function Berita() {
  const Sorotan = (props) => {
    return(
      <Card className="box mb-4 " style={{ width: '40rem'}}>
        <Card.Link href="#hahaha">
          <div>
            <figure className="cards__item__pic-wrap">
              <img src={props.image} alt="Nivea" className="cards__item__img"/>
            </figure>
          </div>
          <Card.Body>
            <Card.Title href="#hahaha/.1" style={{color:"darkblue", textAlign:"center"}} ><b>{props.title}</b></Card.Title>
          </Card.Body>
        </Card.Link>
      </Card>
    );
  }

  return(
    <Jumbotron className="mb-0" style={{backgroundColor:"#f5f5f5"}}>
        <h2 className="mb-5" style={{color:"darkblue", textAlign:"center"}}><b>BERITA & SOROTAN</b></h2>
      <Row >
        <Col className="berita">
          <Sorotan 
            image = {c1}
            title = "LIHAT DISINI"
          />
        </Col>
        <Col className="berita">
          <Sorotan 
            image = {c2}
            title = "KRIM TABIR SURYA YANG TEPAT"
          />
        </Col>
        <Col className="berita">
          <Sorotan 
            image = {c3}
            title = "SPARKLING WHITE WHITENING FOAM"
          />
        </Col>
        <Col className="berita">
          <Sorotan 
            image = {c4}
            title = "APA TIPE KULIT ANDA?"
          />
        </Col>
        <Col className="berita">
          <Sorotan 
            image = {c5}
            title = "KRIM KAMI CARI TAHU DI SINI"
          />
        </Col>
        <Col className="berita">
          <Sorotan 
            image = {c6}
            title = "MEMPUNYAI PERTANYAAN KHUSUS? KONTAK KAMI"
          />
        </Col>
      </Row>
    </Jumbotron>
  );
}


function Sejarah() {
  return(
    <Jumbotron className="mb-0"style={{backgroundColor:"#d6dcec", borderRadius:"0%"}}>
      <h2 className="mb-5" style={{color:"darkblue", textAlign:"center"}}><b>CARING SEJAK 1911</b></h2>
      <Carousel>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100"
            src={sejarah}
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={sejarah2}
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>    
    </Jumbotron>
 
  );
}

function Informasi() {
  return(
    <Jumbotron style={{backgroundColor:"#33428c", marginBottom:"0", borderRadius:"0%"}}>

    </Jumbotron>
  );
}

function Footer() {
  return(
    <Navbar style={{backgroundColor:"#00136f"}}>
      
    </Navbar>
  );
}

function App() {
  return (
    <div>
      <Header/>
      <Slide/>
      <Tentang/>
      <Berita/>
      <Sejarah/>
      <Informasi />
      <Footer />
    </div>
  );
}

export default App;
 